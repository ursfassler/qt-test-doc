FROM ubuntu:18.10

RUN apt-get update -qq

RUN apt-get install -y -qq \
	qt5-qmake \
	qt5-default \
	qtconnectivity5-dev \
	g++

RUN apt-get install -y -qq \
	git \
	cmake \
	g++

ADD gtest.sh /home/root/
RUN /home/root/gtest.sh
RUN rm /home/root/gtest.sh

RUN apt-get install -y -qq \
	git \
	cmake \
	g++ \
	cucumber \
	libboost1.67-all-dev

ADD cucumber-cpp.sh /home/root/
RUN /home/root/cucumber-cpp.sh
RUN rm /home/root/cucumber-cpp.sh

RUN apt-get install -y -qq \
	pandoc \
	texlive

