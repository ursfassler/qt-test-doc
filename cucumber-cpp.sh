#!/bin/sh

set -e

TMPDIR=`mktemp -d`
echo "build cucumber-cpp in ${TMPDIR}"

cd ${TMPDIR}
git clone https://github.com/cucumber/cucumber-cpp.git
cd cucumber-cpp
git checkout f79990879a3c2eea288de6efeefc4008937300cc
mkdir build
cd build
cmake ../
make -j `nproc`
make install
cd
rm -rf ${TMPDIR}

