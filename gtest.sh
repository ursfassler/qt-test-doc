#!/bin/sh

set -e

TMPDIR=`mktemp -d`
echo "build gtest in ${TMPDIR}"

cd ${TMPDIR}
git clone https://github.com/google/googletest.git
cd googletest
git checkout 62ba5d91d0ceff98fbd95294bd94c97017a07f9e
mkdir build
cd build
cmake ../
make -j `nproc`
make install
cd
rm -rf ${TMPDIR}

